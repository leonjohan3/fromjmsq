# Overview
Utility to retrieve text from an ActiveMQ queue, and then print it to stdout.

# Resources
* [Wiki Entry](https://bitbucket.org/leonjohan3/dirmonitor)
