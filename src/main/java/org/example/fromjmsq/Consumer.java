package org.example.fromjmsq;

import java.util.Properties;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

public class Consumer {
    private static final int ARG_COUNT = 2;

    private String providerUrl;
    private String queueName;

    private Consumer() {
    }

    public static void main(final String[] args) {
        new Consumer().run(args);
    }

    private void run(final String[] args) {

        try {

            if (args.length < ARG_COUNT) {
                usage();
            } else {

                parseArgs(args);

                final Properties env = new Properties();
                env.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
                env.put(Context.PROVIDER_URL, providerUrl);
                final Context context = new InitialContext(env);
                final ConnectionFactory connectionFactory = (ConnectionFactory) context.lookup("ConnectionFactory");

                Connection connection = null;
                Session session = null;

                try {

                    connection = connectionFactory.createConnection();
                    session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                    final Destination queue = session.createQueue(queueName);
                    final MessageConsumer consumer = session.createConsumer(queue);
                    connection.start();
                    final Message message = consumer.receive(1_000);

                    if (message instanceof TextMessage) {
                        final TextMessage textMessage = (TextMessage) message;

                        if (!textMessage.getText().isEmpty()) {
                            System.out.print(textMessage.getText());
                        }
                    }

                } finally {
                    if (null != session) {
                        session.close();
                        session = null;
                    }
                    if (null != connection) {
                        connection.close();
                        connection = null;
                    }
                }
            }

        } catch (final Exception e) {
            e.printStackTrace();
            usage();
        }
    }

    private void usage() {
        System.err.println("\nUsage: java -jar fromjmsq-1.0.0-SNAPSHOT.jar providerUrl queueName\n");
        System.err.println("    providerUrl: AciveMQ provider URL, e.g. tcp://localhost:61616");
        System.err.println("    queueName  : the queue name that the next text JMS message will be consumed from, and printed to stdout\n");
        System.exit(1);
    }

    private void parseArgs(final String[] args) {

        for (int i = 0; i < args.length; i++) {

            if (0 == i) {
                providerUrl = args[i];
            } else {
                queueName = args[i];
            }
        }
    }
}
